import React, { Component } from 'react';
import {StyleSheet, View, Image, Text} from 'react-native';
import {StackActions} from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';

class Splash extends Component {
    constructor(props){
        super(props);
        this.state = {};
        
    }

    componentDidMount = () => {
        setTimeout(() => {
            console.log('xxdid mount');
            this.props.navigation.dispatch(StackActions.replace('Login'));
            // this.props.navigation.dispatch(StackActions.replace('Home'));
        }, 4000);
    }

    render() {

        return(
            <SafeAreaView>
             
                    <View style={styles.container}>
                        <Image 
                            source={require('../images/logo-login.png')} 
                            style={styles.logo}
                        />
                        <Text style={[styles.logoName, {marginTop: 10, fontSize: 35}]}>EWISTUDIO</Text>
                        <Text style={[styles.logoName, {marginTop: 0, fontSize: 9}]}>Smart Project</Text>
                    </View>

            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#13CBEC', 
        width:'100%', 
        height: '100%', 
        justifyContent: 'center', 
        alignItems: 'center'
    },

    logoName:{
        color: '#05345E',
        fontWeight: 'bold'
    },

    logo: {
        width: 150, 
        height: 150
    },
  });

export default Splash;