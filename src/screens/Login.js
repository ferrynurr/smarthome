import React, {Component, useState, useEffect} from 'react';
import {Pressable, SafeAreaView, Image, StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import { NavigationContainer, useNavigation } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { 
    NativeBaseProvider, extendTheme, Box, Input, Container, Center, Heading, Flex, FormControl,
    Stack, Link, Button, HStack
} from "native-base";
import axios from 'axios';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {db} from '../config/firebase';
import {ref, set, onValue } from "firebase/database";

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            email: '',
            password : '',
            pwdShow : true,
        };
    }

    _login = () => {
        console.log('ready:', this.state.email);

        set(ref(db, 'users/' + this.state.email), {
            email: this.state.email,
            password: this.state.password
        }).then(() => {
            console.log('sukses');
        }).catch((e) => {
            console.log('gagal');
        });

    }

    _loginWith = (flag) => {
        alert(flag);
    }

    _changeIcon = (id) => {
        console.log('changeIcon:', id);
        if(id == 'pwd')
            this.setState({ pwdShow: this.state.pwdShow ? false : true })

    }

    render() {
        return (
            <NativeBaseProvider>
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Box style={{alignItems: 'center'}}>
                            <Box mb="3">
                                <Image 
                                    source={require('../images/logo-login.png')} 
                                    style={styles.logo}
                                />
                            </Box>
                            <Text style={{fontSize: 30, color: '#04505E', fontWeight: 'bold'}}> 
                                LOGIN
                            </Text>
                        </Box>
                    </View>

                    {/* <FormContent/> */}
                    <Box>
                        <Box style={styles.content}>
                            <FormControl mb="5">
                                <Stack mx="3">
                                    <FormControl.Label>Email</FormControl.Label>
                                    <Input 
                                        type='text'
                                        placeholder="Masukan Email" 
                                        onChangeText={(email) => this.setState({ email })}
                                        value={this.state.email}
                                        style={{fontSize: 17}}
                                    />
                                </Stack>
                            </FormControl>
                            <FormControl>
                                <Stack mx="3">
                                    <FormControl.Label>Kata Sandi</FormControl.Label>
                                    <Input 
                                        secureTextEntry={this.state.pwdShow}
                                        InputRightElement={
                                            <Pressable onPress={() => this._changeIcon('pwd') }>
                                                <Icon 
                                                name={this.state.pwdShow ? "eye-off" : "eye"} 
                                                style={{ marginRight: 10 }} 
                                                size={30} color="#878787"/>
                                            </Pressable>
                                        } 
                                        placeholder="Masukan Kata Sandi" 
                                        onChangeText={(password) => this.setState({ password })}
                                        value={this.state.password}
                                        style={{fontSize: 17}}
                                    />
                                </Stack>
                            </FormControl>
                            <Box mt="5" ml="3" mr="3">
                                <Box alignItems="flex-end" mb="5">
                                    <Link _text={{
                                            fontSize: "md",
                                            _light: {
                                                color: "cyan.500"
                                            },
                                            color: "cyan.300"
                                            }} href="" isUnderlined _hover={{
                                            _text: {
                                                _light: {
                                                color: "cyan.600"
                                                },
                                                color: "cyan.400"
                                            }
                                            }}>
                                                Lupa Kata Sandi?
                                    </Link>
                                </Box>

                                <Box alignItems="center" mt="8">
                                    <Button  style={{width: '100%'}} size="lg" colorScheme="primary" onPress={this._login}>
                                        Masuk
                                    </Button>
                                </Box>
                            </Box>
                        </Box>

                        <Box style={styles.footerContent}>
                            <Stack space={2} alignItems="center">
                                <HStack space={2} alignItems="center">
                                    <Box><Text style={styles.label}>Belum Punya Akun?</Text></Box>
                                    <Box>
                                        <Link _text={{
                                            fontSize: "md",
                                            _light: {
                                                color: "cyan.500"
                                            },
                                            color: "cyan.300"
                                            }} href="" isUnderlined _hover={{
                                            _text: {
                                                _light: {
                                                color: "cyan.600"
                                                },
                                                color: "cyan.400"
                                            }
                                            }}
                                            onPress={() =>  this.props.navigation.navigate('Register') }
                                        >
                                                Daftar
                                        </Link>
                                    </Box>
                                </HStack>
                            </Stack>
                            <Box style={{marginTop: 60,marginLeft:40, marginRight:40, flex:1}}>
                                <Box style={{flexDirection: 'row'}} mb="2">
                                    <Box style={{ marginRight:20, height:2, flex:1, backgroundColor: '#616060', alignSelf: 'center'}}></Box>
                                    <Text style={{color: '#616060', fontWeight: 'bold', fontSize: 14}}>Masuk Dengan</Text>
                                    <Box style={{marginLeft:20, height:2, flex:1, backgroundColor: '#616060', alignSelf: 'center'}}></Box>
                                </Box>
                                <Center>
                                    <Box style={{flexDirection: 'row'}}>
                                        <Pressable style={{margin:12}} onPress={() => this._loginWith('facebook')}>
                                            <Icon name="facebook" size={45} color="#0748C1" />
                                        </Pressable>
                                        <Pressable style={{margin:12}} onPress={() => this._loginWith('google')}>
                                            <Icon name="google" size={45} color="#CE0202" /> 
                                        </Pressable>
                                        <Pressable style={{margin:12}} onPress={() => this._loginWith('twitter')}>
                                            <Icon name="twitter" size={45} color="#02B5CE" /> 
                                        </Pressable>
                                    </Box>
                                </Center>
                            </Box>
                        </Box>
                    </Box>
                </View>
            </NativeBaseProvider>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        // flexDirection: 'row', 
        // flex:1, 
        justifyContent: 'center',  
        // alignItems: 'center',
        backgroundColor: '#13CBEC',
        marginTop: 40,
        // paddingLeft: 10,
        // paddingRight: 10,
        // borderBottomWidth: 5,
        // borderBottomLeftRadius: 35,
        // borderBottomRightRadius: 35,
    },
    content: {
        backgroundColor: '#fff',
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
        width: '100%',
        marginTop: 20,
        paddingBottom: 35,
        paddingTop: 30,
        paddingRight: 20,
        paddingLeft: 20,
    },
    logo:{
        width: 80, 
        height: 80
    },
    container: {
        // flexDirection: 'column',
        // flex:1,
        backgroundColor: '#13CBEC',

    },
    footerContent:{
        backgroundColor: '#fff',
        height: '100%'
    },
    label: {
        color: '#616060',
        fontWeight: 'bold',
        fontSize: 16,
    },
    input: {
        height: 40,
        margin: 12,
        padding: 10,
        borderWidth: 1,
        borderRadius:15,
    },
  });

  
  export default Login;