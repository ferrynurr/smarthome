import React, {Component, useState, useEffect} from 'react';
import {Pressable, SafeAreaView, Image, StyleSheet, Text, View, TextInput, TouchableOpacity, ScrollView } from 'react-native';

import { 
    NativeBaseProvider, extendTheme, Box, Input, Container, Center, Heading, Flex, FormControl,
    Stack, Link, Button, HStack
} from "native-base";
import axios from 'axios';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {db} from '../config/firebase';
import {ref, set, onValue } from "firebase/database";
import Dropdown from '../components/Dropdown';
import InputText from '../components/InputText';

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            fullname: '',
            email: '',
            password : '',
            passwordConf : '',
            paswdShow: true,
            paswdConfShow: true,
            test : '',
        };
    }

    _changeIcon = (id) => {
        console.log('changeIcon:', id);
        if(id == 'pwd')
            this.setState({ paswdShow: this.state.paswdShow ? true: false });
        else if(id == 'pwdConf')
            this.setState({ paswdConfShow: this.state.paswdConfShow ? true : false });

    }

    _changeRegister = () => {
        console.log('ready:', this.state.test);
        alert('sukses mendaftar:' + this.state.fullname);

    }

    render() {

        return (
            <NativeBaseProvider>
                <SafeAreaView>
                    <View style={styles.container}>
                        <ScrollView>
                            <View style={styles.header}>
                                <View style={{alignItems: 'center'}}>
                                    <View>
                                        <Image 
                                            source={require('../images/logo-login.png')} 
                                            style={styles.logo}
                                        />
                                    </View>
                                    <Text style={{fontSize: 30, color: '#04505E', fontWeight: 'bold'}}> 
                                        REGISTRASI
                                    </Text>
                                </View>
                            </View>
                            <View style={styles.content}>  
                                <InputText 
                                    caption="Nama Lengkap" 
                                    type="text" 
                                    value={this.state.fullname}
                                    onChangeText={(fullname) => this.setState({ fullname })}
                                />
                                <InputText 
                                    caption="Email" 
                                    type="text" 
                                    value={this.state.email}
                                    onChangeText={(email) => this.setState({ email })}
                                />    
                                <Dropdown caption="Jenis Kelamin" mode="SCROLLVIEW"/>
                                <InputText
                                    caption="Kata Sandi"
                                    security={this.state.paswdShow}
                                    value={this.state.password}
                                    iconName={this.state.paswdShow ? 'eye-off' : 'eye'}
                                    onChangeBtnIcon={() => this._changeIcon('pwd')}
                                    onChangeText={(password) => this.setState({ password })}
                                />    
                                <FormControl mb="5">
                                    <Stack mx="3">
                                        <FormControl.Label>Kata Sandi</FormControl.Label>
                                        <Input 
                                            secureTextEntry={this.state.paswdShow}
                                            InputRightElement={
                                                <Pressable onPress={() => this._changeIcon('pwd') }>
                                                    <Icon 
                                                        name={this.state.paswdShow ? 'eye-off' : 'eye'} 
                                                        style={{ marginRight: 10 }} 
                                                        size={30} 
                                                        color="#878787"
                                                        />
                                                </Pressable>
                                            } 
                                            placeholder="Masukan Kata Sandi" 
                                            onChangeText={(password) => this.setState({ password })}
                                            value={this.state.password}
                                            style={{fontSize: 16}}
                                        />
                                    </Stack>
                                </FormControl>

                                <FormControl mb="5">
                                    <Stack mx="3">
                                        <FormControl.Label>Konfirmasi Kata Sandi</FormControl.Label>
                                        <Input 
                                            secureTextEntry={this.state.paswdConfShow}
                                            InputRightElement={
                                                <Pressable onPress={() => this._changeIcon('pwdConf') }>
                                                    <Icon 
                                                        name={this.state.paswdConfShow ? 'eye-off' : 'eye'} 
                                                        style={{ marginRight: 10 }} 
                                                        size={30} 
                                                        color="#878787"
                                                        />
                                                </Pressable>
                                            } 
                                            placeholder="Konfirmasi Kata Sandi" 
                                            onChangeText={(passwordConf) => this.setState({ passwordConf })}
                                            value={this.state.passwordConf}
                                            style={{fontSize: 16}}
                                        />
                                    </Stack>
                                </FormControl>
                            
                                <Box mt="3" ml="3" mr="3">
                                    <Box alignItems="center" mt="8">
                                        <Button  style={{width: '100%'}} size="lg" colorScheme="primary" onPress={this._changeRegister}>
                                            Daftar
                                        </Button>
                                    </Box>
                                </Box>
                            </View>
                        </ScrollView>
                    </View>
                </SafeAreaView>
            </NativeBaseProvider>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        // flexDirection: 'row', 
        // flex:1, 
        justifyContent: 'center',  
        // alignItems: 'center',
        backgroundColor: '#13CBEC',
        marginTop: 30,
        // paddingLeft: 10,
        // paddingRight: 10,
        // borderBottomWidth: 5,
        // borderBottomLeftRadius: 35,
        // borderBottomRightRadius: 35,
    },
    content: {
        backgroundColor: '#fff',
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
        width: '100%',
        height: '100%',
        marginTop: 15,
        paddingBottom: 35,
        paddingTop: 25,
        paddingRight: 20,
        paddingLeft: 20,
    },
    logo:{
        width: 80, 
        height: 80
    },
    container: {
        // flexDirection: 'column',
        // flex:1,
        backgroundColor: '#13CBEC',

    },
    footerContent:{
        backgroundColor: '#fff',
        height: '100%'
    },
    label: {
        color: '#616060',
        fontWeight: 'bold',
        fontSize: 16,
    },
    input: {
        height: 40,
        margin: 12,
        padding: 10,
        borderWidth: 1,
        borderRadius:15,
    },
  });

  
  export default Register;