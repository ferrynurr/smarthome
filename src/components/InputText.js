import React, { Component, useState, useEffect } from 'react';
import { Pressable, SafeAreaView, Image, StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import {
    NativeBaseProvider, extendTheme, Box, Input, Container, Center, Heading, Flex, FormControl,
    Stack, Link, Button, HStack
} from "native-base";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';




const InputText = (props) => {
    const [text, onChangeText] = React.useState('');

    const label = props.caption ? props.caption : "Label InputText";
    const placeholder = props.placeholder ? props.placeholder : "Masukan " + props.caption;
    const value = props.value ? props.value : "";
    const secureTextEntry = props.security ? props.security : false;
    const styleCustom = props.style ? props.style :  { fontSize: 15 };
    const onChangeInput = props.onChangeText ? props.onChangeText : "";
    const btnIcon = props.iconName ? props.iconName : "";
    const onChangeBtnIcon = props.onChangeBtnIcon ? props.onChangeBtnIcon : false;

    _changeIconcv = (id) => {
        console.log('changeIcon:', id);
        if (id == 'pwd')
            this.setState({ paswdShow: this.state.paswdShow ? true : false });
        else if (id == 'pwdConf')
            this.setState({ paswdConfShow: this.state.paswdConfShow ? true : false });

    }

    const GenIcon = () => { 
        if (btnIcon) {
            if (onChangeBtnIcon) {
                return (
                    <Pressable onPress={() => this._changeIcon('pwd')}>
                        <Icon
                            name={btnIcon}
                            style={{ marginRight: 10 }}
                            size={30}
                            color="#878787"
                        />
                    </Pressable>
                );
            }else{
                return (
                    <Icon
                        name={btnIcon}
                        style={{ marginRight: 10 }}
                        size={30}
                        color="#878787"
                    />
                );
            }

        }
        else {
            return ("");
        }
      
    };


    console.log('btnIcon: '+btnIcon);
    console.log('onChangeBtnIcon: ');
    console.log('onChangeText: ' + onChangeText);
    return (
        <FormControl mb="3">
            <Stack>
                <FormControl.Label>{label}</FormControl.Label>
                <Input
                    secureTextEntry={secureTextEntry}
                    InputRightElement={
                        <GenIcon/>
                    }
                    placeholder={placeholder}
                    //onChangeText={(password) => this.setState({ password })}
                    //value={value}
                    onChangeText={onChangeText}
                    value={text}
                    style={styleCustom}

                />
            </Stack>
        </FormControl>
    );
}

export default InputText;