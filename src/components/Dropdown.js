import React, {useState } from 'react';
import { Pressable, SafeAreaView, Image, StyleSheet, Text, View, TextInput, TouchableOpacity, ScrollView } from 'react-native';

import DropDownPicker from 'react-native-dropdown-picker';
import {
    NativeBaseProvider, extendTheme, Box, Input, Container, Center, Heading, Flex, FormControl,
    Stack, Link, Button, HStack
} from "native-base";

const Dropdown = (props) => {

    const label = props.caption ? props.caption : "Label Dropdown";
    const placeholder = props.placeholder ? props.placeholder : "Pilih";
    const selectedItem = props.selected ? props.selected : false;
    const dataSet = props.value ? props.value : [];
    const listMode = props.mode ? props.mode : "DEFAULT";

    const [open, setOpen] = useState(false);
    const [value, setValue] = useState(selectedItem);
    const [items, setItems] = useState(dataSet);

    return (
            <FormControl mb="3" style={{ zIndex: 999 }}>
                <Stack>
                    <FormControl.Label>{label}</FormControl.Label>
                    <DropDownPicker
                        open={open}
                        value={value}
                        items={items}
                        setOpen={setOpen}
                        setValue={setValue}
                        setItems={setItems}
                        placeholder={placeholder}
                        listMode={listMode}
                        style={{
                            backgroundColor: "#F6F6F6",
                            borderColor: '#B8B5B5',

                        }}

                    />
                </Stack>
            </FormControl>
    );
}

export default Dropdown;