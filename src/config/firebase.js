// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app';
// import { getFirestore } from 'firebase/firestore';
import { getDatabase } from 'firebase/database';
// import { getFirestore, addDoc, collection } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// const firebaseConfig = {
//     apiKey: "AIzaSyB0c6boH1h3QCByo6_osNz4RkRV3u2OE7A",
//     // authDomain: "ferrydb-32b87-default-rtdb.asia-southeast1.firebasedatabase.app",
//     authDomain: "ferrydb-32b87.firebaseapp.com",
//     projectId: "ferrydb-32b87",
//     storageBucket: "ferrydb-32b87.appspot.com",
//     messagingSenderId: "83134557129",
//     appId: "1:83134557129:web:b12d76bec70f272c7d9618",
// };
const firebaseConfig = {
    apiKey: "AIzaSyB0c6boH1h3QCByo6_osNz4RkRV3u2OE7A",
    authDomain: "ferrydb-32b87.firebaseapp.com",
    databaseURL: "https://ferrydb-32b87-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "ferrydb-32b87",
    storageBucket: "ferrydb-32b87.appspot.com",
    messagingSenderId: "83134557129",
    appId: "1:83134557129:web:b12d76bec70f272c7d9618"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);


// Initialize Cloud Firestore and get a reference to the service
// export const db = getFirestore(app);
// export {app, db, getFirestore, addDoc, collection };
export const db = getDatabase(app);