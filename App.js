import * as React from 'react';
import {Pressable, SafeAreaView, Image, StyleSheet, Text, View, TextInput, TouchableOpacity } from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import HomeScreen from './src/screens/Home';
import SplashScreen from './src/screens/Splash';
import LoginScreen from './src/screens/Login';
import RegisterScreen from './src/screens/Register';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
          <Stack.Navigator initialRouteName='Register'>
            <Stack.Screen 
                name="Splash" 
                component={SplashScreen} 
                options={{
                    title: 'Splash', 
                    headerShown: false
                }}  
            />
            <Stack.Screen name="Login" 
                component={LoginScreen} 
                options={{
                    title: 'Login', 
                    headerShown: false
                }}
            />
            <Stack.Screen 
                name="Register" 
                component={RegisterScreen} 
                options={{
                    title: 'Register',
                    headerStyle: {backgroundColor: '#13CBEC'},
                    headerTintColor: '#034746',
                }} 
            />
            <Stack.Screen 
                name="Home" 
                component={HomeScreen} 
                options={{
                    title: 'Home',
                    headerShown: false
                }}
            />
        </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;